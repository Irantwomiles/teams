package me.iran.teams.team;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import me.iran.teams.Teams;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

public class TeamManager {

	//TODO: Maybe add allies? (Current thought: list of teams that you can loop through and get their players that way)
	
	Teams plugin;
	
	File file = null;
	File tList = null;
	
	public TeamManager(Teams plugin) {
		this.plugin = plugin;
	}
	
	public static ArrayList<Team> teams = new ArrayList<Team>();
	
	public  void loadTeams() {
		
		tList = new File(plugin.getDataFolder(), "teams.yml");
		
		if(tList.exists()) {
			tList = new File(plugin.getDataFolder(), "teams.yml");
			
			new YamlConfiguration();
			
			YamlConfiguration listConfig = YamlConfiguration.loadConfiguration(tList);
			listConfig.get("teams");
			
			List<String> allFac = listConfig.getStringList("teams");

			for(int i = 0; i < allFac.size(); i++) {
				String name = allFac.get(i);
				
				file = new File(plugin.getDataFolder() + "/Teams", name + ".yml");
				new YamlConfiguration();
				
				YamlConfiguration tem = YamlConfiguration.loadConfiguration(file);
				
				for(String t : tem.getConfigurationSection("teams").getKeys(false)) {
				
					OfflinePlayer leader = Bukkit.getOfflinePlayer(UUID.fromString(tem.getString("teams." + t + ".leader")));
					List<String> members = tem.getStringList("teams." + t + ".members");
					List<String> captains = tem.getStringList("teams." + t + ".captains");
					
					int x = tem.getInt("teams." + t + ".home.x");
					int y = tem.getInt("teams." + t + ".home.y");
					int z = tem.getInt("teams." + t + ".home.z");
					String world = tem.getString("teams." + t + ".home.world");
					float pitch = tem.getFloat("teams." + t + ".home.pitch");
					float yaw = tem.getFloat("teams." + t + ".home.yaw");
					
					int x1 = tem.getInt("teams." + t + ".loc1.x");
					int z1 = tem.getInt("teams." + t + ".loc1.z");
					
					int x2 = tem.getInt("teams." + t + ".loc2.x");
					int z2 = tem.getInt("teams." + t + ".loc2.z");
					
					Location loc1 = new Location(Bukkit.getWorld(world), x1, 0, z1);
					Location loc2 = new Location(Bukkit.getWorld(world), x2, 0, z2);
					
					String teamName = tem.getString("teams." + t + ".name");
					
					Team team = new Team(teamName, leader.getUniqueId().toString());
					team.setMemebers(members);
					team.setCaptains(captains);
					
					team.setHomeX(x);
					team.setHomeY(y);
					team.setHomeZ(z);
					team.setHomePitch(pitch);
					team.setHomeYaw(yaw);
					team.setHomeWorld(world);
					team.setLoc1(loc1);
					team.setLoc2(loc2);
					
					team.setTeamName(teamName);
					
					teams.add(team);
				}
			}
			
			System.out.println(ChatColor.GREEN.toString() + ChatColor.BOLD + "[Teams] Loaded in all teams correctly!");
		}
		
	}
	
	public void saveTeams() {
		
		//teams.yml file
		tList = new File(plugin.getDataFolder(), "teams.yml");
		
		if(tList.exists()) {
			tList = new File(plugin.getDataFolder(), "teams.yml");
			
			new YamlConfiguration();
			
			YamlConfiguration listConfig = YamlConfiguration.loadConfiguration(tList);
			listConfig.get("teams");
			
			List<String> t = listConfig.getStringList("teams");
			
			for(String team : t) {
				file = new File(plugin.getDataFolder() + "/Teams", team + ".yml");
				
				file.delete();
			
			}
			
			//clear current list
			t.clear();
			
			for(int i = 0; i < teams.size(); i++) {
				t.add(teams.get(i).getTeamName());
				
			}
			//set new list to config
			listConfig.set("teams", t);
			List<String> newTeam = listConfig.getStringList("teams");
			
			for(String name : newTeam) {
				
				Team team = getTeamByName(name);
				OfflinePlayer leader = Bukkit.getOfflinePlayer(UUID.fromString(team.getTeamLeader()));
				if(!file.exists()) {
					file = new File(plugin.getDataFolder()+ "/Teams", name + ".yml");
					
					new YamlConfiguration();
					
					YamlConfiguration teamConfig = YamlConfiguration.loadConfiguration(file);
					
					teamConfig.createSection("teams." + name + ".name");
					teamConfig.createSection("teams." + name + ".leader");
					teamConfig.createSection("teams." + name + ".captains"); //List
					teamConfig.createSection("teams." + name + ".members"); //List
					teamConfig.createSection("teams." + name + ".home.x"); //Location
					teamConfig.createSection("teams." + name + ".home.y");
					teamConfig.createSection("teams." + name + ".home.z");
					teamConfig.createSection("teams." + name + ".home.world");
					teamConfig.createSection("teams." + name + ".home.pitch");
					teamConfig.createSection("teams." + name + ".home.yaw");
					
					teamConfig.createSection("teams." + name + ".loc1.x");
					teamConfig.createSection("teams." + name + ".loc1.z");
					
					teamConfig.createSection("teams." + name + ".loc2.x");
					teamConfig.createSection("teams." + name + ".loc2.z");
					
					teamConfig.set("teams." + name + ".name", team.getTeamName());
					teamConfig.set("teams." + name + ".leader", leader.getUniqueId().toString());
					
					teamConfig.set("teams." + name + ".captains", team.getTeamCaptains());
					
					//MEMBERS
					teamConfig.set("teams." + name + ".members", team.getTeamMembers());
					
					teamConfig.set("teams." + name + ".home.x", team.getHomeX());
					teamConfig.set("teams." + name + ".home.y", team.getHomeY());
					teamConfig.set("teams." + name + ".home.z", team.getHomeZ());
					teamConfig.set("teams." + name + ".home.pitch", team.getHomePitch());
					teamConfig.set("teams." + name + ".home.yaw", team.getHomeYaw());
					teamConfig.set("teams." + name + ".home.world", team.getHomeWorld());
					
					teamConfig.set("teams." + name + ".loc1.x", team.getLoc1().getBlockX());
					teamConfig.set("teams." + name + ".loc1.z", team.getLoc1().getBlockZ());
					
					teamConfig.set("teams." + name + ".loc2.x", team.getLoc2().getBlockX());
					teamConfig.set("teams." + name + ".loc2.z", team.getLoc2().getBlockZ());
					
					try {
						teamConfig.save(file);
					} catch (IOException e) {
						e.printStackTrace();
					}
				} else {
					file = new File(plugin.getDataFolder()+ "/Teams", name + ".yml");
					
					new YamlConfiguration();
					
					YamlConfiguration teamConfig = YamlConfiguration.loadConfiguration(file);
					
					teamConfig.createSection("teams." + name);
					teamConfig.createSection("teams." + name + ".name");
					teamConfig.createSection("teams." + name + ".leader");
					teamConfig.createSection("teams." + name + ".captains"); //List
					teamConfig.createSection("teams." + name + ".members"); //List
					teamConfig.createSection("teams." + name + ".home.x"); //Location
					teamConfig.createSection("teams." + name + ".home.y");
					teamConfig.createSection("teams." + name + ".home.z");
					teamConfig.createSection("teams." + name + ".home.world");
					teamConfig.createSection("teams." + name + ".home.pitch");
					teamConfig.createSection("teams." + name + ".home.yaw");
					
					teamConfig.createSection("teams." + name + ".loc1.x");
					teamConfig.createSection("teams." + name + ".loc1.z");
					
					teamConfig.createSection("teams." + name + ".loc2.x");
					teamConfig.createSection("teams." + name + ".loc2.z");
					
					teamConfig.set("teams." + name + ".name", team.getTeamName());
					teamConfig.set("teams." + name + ".leader", leader.getUniqueId().toString());
					
					teamConfig.set("teams." + name + ".captains", team.getTeamCaptains());
					
					//MEMBERS
					teamConfig.set("teams." + name + ".members", team.getTeamMembers());
					
					teamConfig.set("teams." + name + ".home.x", team.getHomeX());
					teamConfig.set("teams." + name + ".home.y", team.getHomeY());
					teamConfig.set("teams." + name + ".home.z", team.getHomeZ());
					teamConfig.set("teams." + name + ".home.pitch", team.getHomePitch());
					teamConfig.set("teams." + name + ".home.yaw", team.getHomeYaw());
					teamConfig.set("teams." + name + ".home.world", team.getHomeWorld());
					
					teamConfig.set("teams." + name + ".loc1.x", team.getLoc1().getBlockX());
					teamConfig.set("teams." + name + ".loc1.z", team.getLoc1().getBlockZ());
					
					teamConfig.set("teams." + name + ".loc2.x", team.getLoc2().getBlockX());
					teamConfig.set("teams." + name + ".loc2.z", team.getLoc2().getBlockZ());
					
					
					try {
						teamConfig.save(file);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				
				//Avoid memory leaks
				team.getInvites().clear();
				
			}
			
			System.out.println("t size " + t.size());
			try {
				listConfig.save(tList);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			System.out.println("set new team list");
		}
	}
	
	public void createTeam(String name, Player leader) {
		
		if(isInTeam(leader)) {
			leader.sendMessage("You are already in a team");
			return;
		}
		
		String[] notAllowed = { ",", ";", "!", "@", "#", "$",
				"%", "^", "&", "*", "(", ")", "+", "=", "`",
				"~", ".", "<", ">", "/", "\"", ":", ";", "{",
				"}", "?" };
		for(String no : notAllowed) {
			if (name.contains(no)) {
				leader.sendMessage(ChatColor.GOLD + "[Teams]" + ChatColor.RED + " Invalid Characters in Team Name");
				return;
			}
		}
		
		if(name.length() < 3 || name.length() > 12) {
			leader.sendMessage(ChatColor.GOLD + "[Teams]" + ChatColor.RED + " Team name has to be atleast 3 letters but no more than 12");
			return;
		}
		
		Team team = new Team(name, leader.getUniqueId().toString());
		
		team.setHomeX(0);
		team.setHomeY(0);
		team.setHomeZ(0);
		team.setHomePitch(0);
		team.setHomeYaw(0);
		team.setHomeWorld(leader.getWorld().getName());
		
		
		file = new File(plugin.getDataFolder() + "/Teams", name + ".yml");
		
		tList = new File(plugin.getDataFolder(), "teams.yml");
		
		
		//List of Teams
		if(!tList.exists()) {
			tList = new File(plugin.getDataFolder(), "teams.yml");
			new YamlConfiguration();
			
			YamlConfiguration listConfig = YamlConfiguration.loadConfiguration(tList);
			listConfig.createSection("teams");
			
			List<String> allTeams = listConfig.getStringList("teams");
			
			allTeams.add(name);
			listConfig.set("teams", allTeams);
			
			try {
				listConfig.save(tList);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			tList = new File(plugin.getDataFolder(), "teams.yml");
			new YamlConfiguration();
			
			YamlConfiguration listConfig = YamlConfiguration.loadConfiguration(tList);
			
			List<String> allTeams = listConfig.getStringList("teams");
		
			
			if(teams.contains(getTeamByName(name))) {
				if(leader.isOnline()) {
					((CommandSender) leader).sendMessage(ChatColor.GOLD + "[Teams]" + ChatColor.RED + " That Team already exist!");
				return;
				}
			}
			
			allTeams.add(name);
			listConfig.set("teams", allTeams);
			
			try {
				listConfig.save(tList);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		//Actual faction data
		if(!file.exists()) {
			file = new File(plugin.getDataFolder()+ "/Teams", name + ".yml");
			
			new YamlConfiguration();
			
			YamlConfiguration teamConfig = YamlConfiguration.loadConfiguration(file);
			
			teamConfig.createSection("teams." + name + ".name");
			teamConfig.createSection("teams." + name + ".leader");
			teamConfig.createSection("teams." + name + ".captains"); //List
			teamConfig.createSection("teams." + name + ".members"); //List
			teamConfig.createSection("teams." + name + ".home"); //Location
			
			teamConfig.createSection("teams." + name + ".loc1.x");
			teamConfig.createSection("teams." + name + ".loc1.z");
			
			teamConfig.createSection("teams." + name + ".loc2.x");
			teamConfig.createSection("teams." + name + ".loc2.z");
			
			
			teamConfig.set("teams." + name + ".name", name);
			teamConfig.set("teams." + name + ".leader", leader.getUniqueId().toString());
			
			List<String> captains = teamConfig.getStringList("teams." + name + ".captains");
			teamConfig.set("teams." + name + ".captains", captains);
			
			//MEMBERS
			List<String> members = teamConfig.getStringList("teams." + name + ".members");
			members.add(leader.getUniqueId().toString());
			teamConfig.set("teams." + name + ".members", members);
			
			teamConfig.set("teams." + name + ".home", null);
			teamConfig.set("teams." + name + ".loc1.x", null);
			teamConfig.set("teams." + name + ".loc1.z", null);
			
			teamConfig.set("teams." + name + ".loc2.x", null);
			teamConfig.set("teams." + name + ".loc2.z", null);
			
			try {
				teamConfig.save(file);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			file = new File(plugin.getDataFolder()+ "/Teams", name + ".yml");
			
			new YamlConfiguration();
			
			YamlConfiguration teamConfig = YamlConfiguration.loadConfiguration(file);
			
			teamConfig.createSection("teams." + name);
			teamConfig.createSection("teams." + name + ".name");
			teamConfig.createSection("teams." + name + ".leader");
			teamConfig.createSection("teams." + name + ".captains"); //List
			teamConfig.createSection("teams." + name + ".members"); //List
			teamConfig.createSection("teams." + name + ".home"); //Location
			
			teamConfig.createSection("teams." + name + ".loc1.x");
			teamConfig.createSection("teams." + name + ".loc1.z");
			
			teamConfig.createSection("teams." + name + ".loc2.x");
			teamConfig.createSection("teams." + name + ".loc2.z");
			
			teamConfig.set("teams." + name + ".name", name);
			teamConfig.set("teams." + name + ".leader", leader.getUniqueId().toString());
			
			List<String> captains = teamConfig.getStringList("teams." + name + ".captains");
			teamConfig.set("teams." + name + ".captains", captains);
			
			List<String> members = teamConfig.getStringList("teams." + name + ".members");
			members.add(leader.getUniqueId().toString());
			teamConfig.set("teams." + name + ".members", members);
			
			teamConfig.set("teams." + name + ".home", null);
			
			teamConfig.set("teams." + name + ".home", null);
			teamConfig.set("teams." + name + ".loc1.x", null);
			teamConfig.set("teams." + name + ".loc1.z", null);
			
			teamConfig.set("teams." + name + ".loc2.x", null);
			teamConfig.set("teams." + name + ".loc2.z", null);
			
			try {
				teamConfig.save(file);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		((CommandSender) leader).sendMessage(ChatColor.WHITE + "You have created the Team " + ChatColor.YELLOW.toString() + ChatColor.BOLD + name);
		teams.add(team);
	}
	
	@SuppressWarnings("deprecation")
	public void joinTeam(Player player, String name) {
		Team team = getTeamByName(name); //Joining Team
		Team playerTeam = getTeamByPlayer(player); //Current team
		
		//is other team valid
		if(!teams.contains(team)) {
			player.sendMessage(ChatColor.GOLD + "[Teams]" + ChatColor.RED + " That Team doesn't exist!");
			return;
		}
		
		if(playerTeam == team) {
			player.sendMessage(ChatColor.GOLD + "[Teams]" + ChatColor.RED + " You can't join your own Team");
			return;
		}
		
		//Does the player have a team
		if(teams.contains(playerTeam)) {
			player.sendMessage(ChatColor.GOLD + "[Teams]" + ChatColor.RED + " You must leave your Current Team to join another!");
			return;
		}
		//Is he invited
		if(!team.getInvites().contains(player.getUniqueId().toString())) {
			player.sendMessage(ChatColor.GOLD + "[Teams]" + ChatColor.RED + " You have not been invited to join this Team");
			return;
		}
		
		if(team.getTeamMembers().size() > 2) {
			player.sendMessage(ChatColor.GOLD + "[Teams]" + ChatColor.RED + " That Team has reached its Maximum number of Players!");
			return;
		}
		
		team.getInvites().remove(player.getUniqueId().toString());
		team.getTeamMembers().add(player.getUniqueId().toString());
		
		for(Player p : Bukkit.getOnlinePlayers()) {
			if(team.getTeamMembers().contains(p.getUniqueId().toString())) {
				p.sendMessage(ChatColor.GREEN + player.getName() + " has joined the Team");
			}
		}
		
	}
	
	@SuppressWarnings("deprecation")
	public void inviteToTeam(Player player, Player target) {
		Team team = getTeamByPlayer(player);
		
		if(!teams.contains(team)) {
			player.sendMessage(ChatColor.GOLD + "[Teams]" + ChatColor.RED + " You are not in a Team");
			return;
		}
		
		if(!team.getTeamLeader().equals(player.getUniqueId().toString()) && !team.getTeamCaptains().contains(player.getUniqueId().toString())) {
			player.sendMessage(ChatColor.GOLD + "[Teams]" + ChatColor.RED + " Only Team Leaders and Captains can do this command");
			return;
		}
		
		if(team.getTeamMembers().contains(target.getUniqueId().toString())) {
			player.sendMessage(ChatColor.GOLD + "[Teams]" + ChatColor.RED + " That person is already in your Team");
			return;
		}
		
		if(team.getInvites().contains(target.getUniqueId().toString())) {
			player.sendMessage(ChatColor.GOLD + "[Teams]" + ChatColor.RED + " That person has already been invited to your Team");
			return;
		}
		
		team.addInvite(target.getUniqueId().toString());
		
		for(Player p : Bukkit.getOnlinePlayers()) {
			if(team.getTeamMembers().contains(p.getUniqueId().toString())) {
				p.sendMessage(ChatColor.YELLOW + player.getName() + " has invited " + target.getName() + " to join the Team!");
			}
		}
		target.sendMessage(ChatColor.YELLOW + "You have been invited to join the team " + team.getTeamName());
	}
	
	@SuppressWarnings("deprecation")
	public void unInvite(Player player, Player target) {
		
		Team team = getTeamByPlayer(player);
		
		if(!teams.contains(team)) {
			player.sendMessage(ChatColor.GOLD + "[Teams]" + ChatColor.RED + " You are not in a Team");
			return;
		}
		
		if(!team.getTeamLeader().equals(player.getUniqueId().toString()) && !team.getTeamCaptains().contains(player.getUniqueId().toString())) {
			player.sendMessage(ChatColor.GOLD + "[Teams]" + ChatColor.RED + " Only Team Leaders and Captains can do this command");
			return;
		}
		
		if(team.getTeamMembers().contains(target.getUniqueId().toString())) {
			player.sendMessage(ChatColor.GOLD + "[Teams]" + ChatColor.RED + " That person is already in your Team");
			return;
		}
		
		if(!team.getInvites().contains(target.getUniqueId().toString())) {
			player.sendMessage(ChatColor.GOLD + "[Teams]" + ChatColor.RED + " That person has not been invited to your Team");
			return;
		}
		
		team.getInvites().remove(target.getUniqueId().toString());
		
		for(Player p : Bukkit.getOnlinePlayers()) {
			if(team.getTeamMembers().contains(p.getUniqueId().toString())) {
				p.sendMessage(ChatColor.YELLOW + player.getName() + " has revoked access for " + target.getName() + " to join the Team!");
			}
		}
	}
	
	@SuppressWarnings("deprecation")
	public void kickFromTeam(Player player, Player target) {
		Team team = getTeamByPlayer(player);
		
		if(!teams.contains(team)) {
			player.sendMessage(ChatColor.GOLD + "[Teams]" + ChatColor.RED + " You are not in a Team");
			return;
		}
		
		if(!team.getTeamLeader().equals(player.getUniqueId().toString()) && !team.getTeamCaptains().contains(player.getUniqueId().toString())) {
			player.sendMessage(ChatColor.GOLD + "[Teams]" + ChatColor.RED + " Only Team Leaders and Captains can do this command");
			return;
		}
		
		if(!team.getTeamMembers().contains(target.getUniqueId().toString())) {
			player.sendMessage(ChatColor.GOLD + "[Teams]" + ChatColor.RED + " That person is not in your Team");
			return;
		}
		
		if(team.getTeamLeader().equals(target.getUniqueId().toString())) {
			player.sendMessage(ChatColor.GOLD + "[Teams]" + ChatColor.RED + " You can't kick your Leader from the Team");
			return;
		}
		
		team.getTeamMembers().remove(target.getUniqueId().toString());
		
		if(team.getTeamCaptains().contains(target.getUniqueId().toString())) {
			team.getTeamCaptains().remove(target.getUniqueId().toString());
		}
		
		for(Player p : Bukkit.getOnlinePlayers()) {
			if(team.getTeamMembers().contains(p.getUniqueId().toString())) {
				p.sendMessage(ChatColor.RED + player.getName() + " has kicked " + target.getName() + " from the Team!");
			}
		}
		target.sendMessage(ChatColor.RED + "You have been Kicked to from the team " + team.getTeamName());
	}
	
	public void claimLand(Player player) {
		Team team = getTeamByPlayer(player);
		
		if(!teams.contains(team)) {
			player.sendMessage(ChatColor.GOLD + "[Teams]" + ChatColor.RED + " You are not in a Team");
			return;
		}
		
		
	}
	
	@SuppressWarnings("deprecation")
	public void makeLeader(Player player, Player target) {
		Team team = getTeamByPlayer(player);
		
		if(!teams.contains(team)) {
			player.sendMessage(ChatColor.GOLD + "[Teams]" + ChatColor.RED + " You are not in a Team");
			return;
		}
		
		if(!team.getTeamLeader().equals(player.getUniqueId().toString())) {
			player.sendMessage(ChatColor.GOLD + "[Teams]" + ChatColor.RED + " You are not the Team Leader");
			return;
		}
		
		if(!team.getTeamMembers().contains(target.getUniqueId().toString())) {
			player.sendMessage(ChatColor.GOLD + "[Teams]" + ChatColor.RED + " That person is not in your Team");
			return;
		}
		
		team.setTeamLeader(target.getUniqueId().toString());
		
		for(Player p : Bukkit.getOnlinePlayers()) {
			if(team.getTeamMembers().contains(p.getUniqueId().toString())) {
				p.sendMessage(ChatColor.LIGHT_PURPLE.toString() + ChatColor.UNDERLINE + player.getName() + " has made " + target.getName() + " the new Team Leader!");
			}
		}
	}
	
	public void promotePlayer(Player player, Player target) {
		
	}
	
	public void demotePlayer(Player player, Player target) {
		Team team = getTeamByPlayer(player);
		
		if(!getAllTeams().contains(team)) {
			//not in a team
			return;
		}
		
		if(!team.getTeamLeader().equals(player.getUniqueId().toString())) {
			//not leader
			return;
		}
		
		if(!team.getTeamMembers().contains(target.getUniqueId().toString())) {
			//Not in the same team as leader
			return;
		}
		
		if(!team.getTeamCaptains().contains(target.getUniqueId().toString())) {
			//Not captain
			return;
		}
	}
	
	public Team getTeamByName(String name) {
		for(Team team : teams) {
			if(team.getTeamName().equalsIgnoreCase(name)) {
				return team;
			}
		}
		return null;
	}
	
	
	public Team getTeamByPlayer(Player player) {
		for(Team team : teams) {
			if(team.getTeamMembers().contains(player.getUniqueId().toString())) {
				return team;
			}
		}
		return null;
	}
	
	public boolean isInTeam(Player player) {
		for(Team team : teams) {
			if(team.getTeamMembers().contains(player.getUniqueId().toString())) {
				return true;
			}
		}
		return false;
	}
	
	@SuppressWarnings("deprecation")
	public void leaveTeam(Player player) {
		Team team = getTeamByPlayer(player);
		
		if(!teams.contains(team)) {
			player.sendMessage(ChatColor.GOLD + "[Teams]" + ChatColor.RED + " You are not in a team");
			return;
		}
		
		if(team.getTeamMembers().size() == 1) {
			Bukkit.broadcastMessage(ChatColor.RED + player.getName() + " has disbanded the team " + team.getTeamName());
			team.getTeamCaptains().clear();
			team.getTeamMembers().clear();
			teams.remove(team);
			return;
		}
		
		if(team.getTeamLeader().equals(player.getUniqueId().toString())) {
			player.sendMessage(ChatColor.GOLD + "[Teams]" + ChatColor.RED + " You must make someone the leader before leaving or /team disband");
			return;
		}
		
		if(team.getTeamCaptains().contains(player.getUniqueId().toString()) && team.getTeamMembers().contains(player.getUniqueId().toString())) {
			team.getTeamCaptains().remove(player.getUniqueId().toString());
			team.getTeamMembers().remove(player.getUniqueId().toString());
			for(Player p : Bukkit.getOnlinePlayers()) {
				if(team.getTeamMembers().contains(p.getUniqueId().toString())) {
					p.sendMessage(ChatColor.RED + player.getName() + " Has left the Team!");
				}
			}
			
			player.sendMessage(ChatColor.RED + "You have left the Team");
			return;
		}
		
		if(team.getTeamMembers().contains(player.getUniqueId().toString())) {
			team.getTeamMembers().remove(player.getUniqueId().toString());
			for(Player p : Bukkit.getOnlinePlayers()) {
				if(team.getTeamMembers().contains(p.getUniqueId().toString())) {
					p.sendMessage(ChatColor.RED + player.getName() + " Has left the Team!");
				}
			}
			player.sendMessage(ChatColor.RED + "You have left the Team");
			return;
		}
		
	}
	
	public void disbandTeam(Player player) {
		Team team = getTeamByPlayer(player);
		
		if(!teams.contains(team)) {
			player.sendMessage(ChatColor.GOLD + "[Teams]" + ChatColor.RED + " You are not in a team");
			return;
		}
		
		if(team.getTeamLeader().equals(player.getUniqueId().toString())) {
			team.getTeamCaptains().clear();
			team.getTeamMembers().clear();
			teams.remove(team);
		}
		
		Bukkit.broadcastMessage(player.getName() + " has disbanded the team " + team.getTeamName());
	}
	
	public void getTeamInfoByName(Player player, String name) {
		
		Team team = getTeamByName(name);
		
		if(!teams.contains(team)) {
			player.sendMessage(ChatColor.GOLD + "[Teams]" + ChatColor.RED + " That Team doesn't exist");
			return;
		}

		List<String> everyone = team.getTeamMembers();
		List<String> online = new ArrayList<String>();
		List<String> offline = new ArrayList<String>();
		for(int i = 0; i < everyone.size(); i++) {
			OfflinePlayer on = Bukkit.getOfflinePlayer(UUID.fromString(everyone.get(i)));
			
			if(on.isOnline()) {
				online.add(on.getUniqueId().toString());
			} else {
				offline.add(on.getUniqueId().toString());
			}
		}
		
		OfflinePlayer leader = Bukkit.getOfflinePlayer(UUID.fromString(team.getTeamLeader()));
		//[Team]
		//Home location x, y, z
		//Team Leader: p
		//team captains: p
		//Online Players (x/x): 
		//Offline Players (y/y):
		player.sendMessage("");
		player.sendMessage(ChatColor.GOLD + "[" + name + "]");
		
		StringBuilder onlineList = new StringBuilder();
		
		for(int i = 0; i < online.size(); i++) {
			onlineList = onlineList.append(Bukkit.getOfflinePlayer(UUID.fromString(online.get(i))).getName() + ", ");
		}

		StringBuilder offlineList = new StringBuilder();
		
		for(int i = 0; i < offline.size(); i++) {
			offlineList = offlineList.append(Bukkit.getOfflinePlayer(UUID.fromString(offline.get(i))).getName() + ", ");
		}
		
		//Leader
		if(leader.isOnline()) {
			player.sendMessage(ChatColor.YELLOW + "[Leader] " + ChatColor.GREEN + leader.getName());
		} else {
			player.sendMessage(ChatColor.YELLOW + "[Leader] " + ChatColor.RED + leader.getName());
		}
		
		
		player.sendMessage(ChatColor.YELLOW + "Online players (" + online.size() + "/" + "15" + "): " + ChatColor.GREEN + onlineList);
		player.sendMessage(ChatColor.YELLOW + "Offline players (" + offline.size() + "/" + "15" + "): " + ChatColor.RED + offlineList);
		player.sendMessage("");
	}
	
	public void getTeamInfoByPlayer(Player player, Player target) {
		
		Team team = getTeamByPlayer(target);
		
		if(!teams.contains(team)) {
			player.sendMessage(ChatColor.GOLD + "[Teams]" + ChatColor.RED + " That Team doesn't exist");
			return;
		}

		List<String> everyone = team.getTeamMembers();
		List<String> online = new ArrayList<String>();
		List<String> offline = new ArrayList<String>();
		for(int i = 0; i < everyone.size(); i++) {
			OfflinePlayer on = Bukkit.getOfflinePlayer(UUID.fromString(everyone.get(i)));
			
			if(on.isOnline()) {
				online.add(on.getUniqueId().toString());
			} else {
				offline.add(on.getUniqueId().toString());
			}
		}
		
		OfflinePlayer leader = Bukkit.getOfflinePlayer(UUID.fromString(team.getTeamLeader()));
		//[Team]
		//Home location x, y, z
		//Team Leader: p
		//team captains: p
		//Online Players (x/x): 
		//Offline Players (y/y):
		player.sendMessage("");
		player.sendMessage(ChatColor.GOLD + "[" + team.getTeamName() + "]");
		
		StringBuilder onlineList = new StringBuilder();
		
		for(int i = 0; i < online.size(); i++) {
			onlineList = onlineList.append(Bukkit.getOfflinePlayer(UUID.fromString(online.get(i))).getName() + ", ");
		}

		StringBuilder offlineList = new StringBuilder();
		
		for(int i = 0; i < offline.size(); i++) {
			offlineList = offlineList.append(Bukkit.getOfflinePlayer(UUID.fromString(offline.get(i))).getName() + ", ");
		}
		
		//Leader
		if(leader.isOnline()) {
			player.sendMessage(ChatColor.YELLOW + "[Leader] " + ChatColor.GREEN + leader.getName());
		} else {
			player.sendMessage(ChatColor.YELLOW + "[Leader] " + ChatColor.RED + leader.getName());
		}
		
		
		player.sendMessage(ChatColor.YELLOW + "Online players (" + online.size() + "/" + "15" + "): " + ChatColor.GREEN + onlineList);
		player.sendMessage(ChatColor.YELLOW + "Offline players (" + offline.size() + "/" + "15" + "): " + ChatColor.RED + offlineList);
		player.sendMessage("");
	}
	
	public Team insideClaim(Location loc) {
		
		for(Team team : teams) {
			
			Location loc1 = team.getLoc1();
			Location loc2 = team.getLoc2();

			int xMax = Math.max(loc1.getBlockX(), loc2.getBlockX());
			int zMax = Math.max(loc1.getBlockZ(), loc2.getBlockZ());

			int xMin = Math.min(loc1.getBlockX(), loc2.getBlockX());
			int zMin = Math.min(loc1.getBlockZ(), loc2.getBlockZ());

			if ((loc.getBlockX() >= xMin) && (loc.getBlockX() <= xMax)) {
				if ((loc.getBlockZ() >= zMin) && (loc.getBlockZ() <= zMax)) {
					return team;
				}
			}
		}
		
		return null;
	}
	
	public List<Team> getAllTeams() {
		return TeamManager.teams;
	}

	
}
