package me.iran.teams.team;

import java.util.HashMap;

import me.iran.teams.Teams;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

public class ClaimEvent implements Listener {

	Teams plugin;
	
	TeamManager tm = new TeamManager(plugin);
	
	public static HashMap<String, Claim> claim = new HashMap<>();
	
	public ClaimEvent (Teams plugin) {
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onClaim(PlayerInteractEvent event) {
		Player player = event.getPlayer();
		

		if (event.getAction() == null) {
			return;
		}

		if(event.getAction() == Action.LEFT_CLICK_BLOCK && player.getItemInHand().getType() == Material.WOOD_HOE) {
			if(claim.containsKey(player.getName())) {
				if(tm.isInTeam(player)) {
					Team team = tm.getTeamByPlayer(player);
					
					if(team.getTeamLeader().equals(player.getUniqueId().toString())) {
						claim.get(player.getName()).setLoc1(event.getClickedBlock().getLocation());
						player.sendMessage(ChatColor.GREEN + "Set location 1");
					}
					
				}
			}
		} else if(event.getAction() == Action.RIGHT_CLICK_BLOCK && player.getItemInHand().getType() == Material.WOOD_HOE) {
			if(claim.containsKey(player.getName())) {
				if(tm.isInTeam(player)) {
					Team team = tm.getTeamByPlayer(player);
					
					if(team.getTeamLeader().equals(player.getUniqueId().toString())) {
						claim.get(player.getName()).setLoc2(event.getClickedBlock().getLocation());
						player.sendMessage(ChatColor.GREEN + "Set location 2");
					}
					
				}
			}
		} else if(event.getAction() == Action.LEFT_CLICK_AIR && player.isSneaking() && player.getItemInHand().getType() == Material.WOOD_HOE) {
			if(claim.containsKey(player.getName())) {
				
				if(claim.get(player.getName()).getLoc1() != null && claim.get(player.getName()).getLoc2() != null) {
					if(tm.isInTeam(player)) {
						Team team = tm.getTeamByPlayer(player);
						
						if(team.getTeamLeader().equals(player.getUniqueId().toString())) {
							team.setLoc1(claim.get(player.getName()).getLoc1());
							team.setLoc2(claim.get(player.getName()).getLoc2());
							
							player.sendMessage(ChatColor.GOLD + "Land claimed");
							
							claim.remove(player.getName());
						}
						
					}
				}
			}
		}
		
	}
	
}
