package me.iran.teams.team;

import org.bukkit.Location;

public class Claim {

	private Location loc1;
	private Location loc2;
	
	private Team team;
	
	public Claim(Location loc1, Location loc2, Team team) {
		this.loc1 = loc1;
		this.loc2 = loc2;
		this.team = team;
	}
	
	public Location getLoc1() {
		return loc1;
	}
	
	public Location getLoc2() {
		return loc2;
	}
	
	public Team getTeam() {
		return team;
	}
	
	public void setLoc1(Location loc1) {
		this.loc1 = loc1;
	}
	
	public void setLoc2(Location loc2) {
		this.loc2 = loc2;
	}
	
	public void setTeam(Team team) {
		this.team = team;
	}
}
