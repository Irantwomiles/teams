package me.iran.teams.team;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Location;


public class Team {

	private String name;
	private String leader;
	private List<String> members;
	private List<String> invites;
	private List<String> captains;
	private int x;
	private int y;
	private int z;
	private float yaw;
	private float pitch;
	private String world;
	private Location loc1 = null;
	private Location loc2 = null;
	private double dtr;
	private boolean regen;
	
	public Team(String name, String leader) {
		this.name = name;
		this.leader = leader;
		
		members = new ArrayList<String>();
		captains = new ArrayList<String>();
		invites = new ArrayList<String>();
		
		dtr = 0.0;
		
		regen = false;
		
		members.add(leader);
	}

	public void setRegen(boolean regen) {
		this.regen = regen;
	}
	
	public boolean isRegen() {
		return this.regen;
	}
	
	public void setDtr(double dtr) {
		this.dtr = dtr;
	}
	
	public double getDtr() {
		return this.dtr;
	}
	
	public Location getLoc1() {
		return this.loc1;
	}
	
	public Location getLoc2() {
		return this.loc2;
	}
	
	public void setLoc1(Location loc1) {
		this.loc1 = loc1;
	}
	
	public void setLoc2(Location loc2) {
		this.loc2 = loc2;
	}
	
	public int getHomeX() {
		return x;
	}
	
	public int getHomeY() {
		return y;
	}
	
	public int getHomeZ() {
		return z;
	}
	
	public float getHomeYaw() {
		return yaw;
	}
	
	public float getHomePitch() {
		return pitch;
	}
	
	public String getHomeWorld() {
		return world;
	}
	
	public void setHomeX(int x) {
		this.x = x;
	}
	public void setHomeY(int y) {
		this.y = y;
	}
	public void setHomeZ(int z) {
		this.z = z;
	}
	
	public void setHomePitch(float pitch) {
		this.pitch = pitch;
	}
	
	public void setHomeYaw(float yaw) {
		this.yaw = yaw;
	}
	
	public void setHomeWorld(String world) {
		this.world = world;
	}
	
	public String getTeamName() {
		return name;
	}
	
	public String getTeamLeader() {
		return leader;
	}
	
	public List<String> getTeamMembers() {
		return members;
	}
	
	public List<String> getTeamCaptains() {
		return captains;
	}
	
	public List<String> getInvites() {
		return invites;
	}
	
	public void setTeamLeader(String leader) {
		this.leader = leader;
	}
	
	public void setTeamName(String name) {
		this.name = name;
	}
	
	
	public void setMemebers(List<String> members) {
		this.members = members;
	}
	
	public void setCaptains(List<String> captains) {
		this.captains = captains;
	}
	
	public void setInvites(List<String> invites) {
		this.invites = invites;
	}
	
	public void addMember(String uuid) {
		this.members.add(uuid);
	}
	
	public void addCaptain(String uuid) {
		this.captains.add(uuid);
	}
	
	public void addInvite(String uuid) {
		this.invites.add(uuid);
	}
	
	public void removeCaptain(String uuid) {
		this.captains.remove(uuid);
	}
	
	public void removeMember(String uuid) {
		this.members.remove(uuid);
	}
}
