package me.iran.teams.team;

import java.util.HashMap;

import me.iran.teams.Teams;
import me.iran.teams.kits.Bard;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class TeamKits implements Listener {

	Teams plugin;
	TeamManager tm = new TeamManager(plugin);
	
	public TeamKits (Teams plugin) {
		this.plugin = plugin;
	}
	
	HashMap<String, Long> kitCooldown = new HashMap<String, Long>();
	
	Bard bard = new Bard();
	
	@EventHandler
	public void onRightClick(PlayerInteractEvent event) {
		if(event.getAction() == null) {
			return;
		}
		
		Player player = event.getPlayer();
		
		if(bard.getBard().contains(player.getName())) {
			Team team = tm.getTeamByPlayer(player);
			if(tm.getAllTeams().contains(team)) {
				
				if(event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK) {
					if(player.getInventory().getItemInHand() == null) {
						return;
					}
					
					//Jump Boost
					if(player.getItemInHand().getType() == Material.FEATHER) {
						
						if(kitCooldown.containsKey(player.getName()) && kitCooldown.get(player.getName()) > System.currentTimeMillis()) {
							
							long timer = kitCooldown.get(player.getName()) - System.currentTimeMillis();
							player.sendMessage(ChatColor.RED.toString() + "Can't used Bard Effects for another " + ChatColor.BOLD + timer/1000 + "'s");
							
						} else {
							kitCooldown.put(player.getName(), System.currentTimeMillis() + (10 * 1000));
							player.sendMessage(ChatColor.GOLD + "Jump Boost Effect Used -> " + ChatColor.RED + "Cooldown: 20 Seconds");
							player.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 200, 1));
							for(Entity p : player.getNearbyEntities(10, 10, 10)) {
							if(p instanceof Player) {
								Player near = (Player) p;
								if(team.getTeamMembers().contains(near.getUniqueId().toString())) {
									near.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 200, 1));
									near.sendMessage(ChatColor.YELLOW + player.getName() + " has just gave " + ChatColor.GOLD + "Jump Boost " + ChatColor.YELLOW + "for" + ChatColor.GOLD + " 10 Seconds.");
									
									}
								}
							}
							
							if(player.getItemInHand().getAmount() > 1) {
								player.getItemInHand().setAmount(player.getItemInHand().getAmount() - 1);
							} else {
								player.setItemInHand(null);
							}
						}
					}
					
					//Strength
					if(player.getItemInHand().getType() == Material.BLAZE_POWDER) {
						
						if(kitCooldown.containsKey(player.getName()) && kitCooldown.get(player.getName()) > System.currentTimeMillis()) {
							
							long timer = kitCooldown.get(player.getName()) - System.currentTimeMillis();
							player.sendMessage(ChatColor.RED.toString() + "Can't used Bard Effects for another " + ChatColor.BOLD + timer/1000 + "'s");
							
						} else {
							kitCooldown.put(player.getName(), System.currentTimeMillis() + (30 * 1000));
							player.sendMessage(ChatColor.GOLD + "Strength Effect Used -> " + ChatColor.RED + "Cooldown: 30 Seconds");
							player.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 20 * 5, 1));
							for(Entity p : player.getNearbyEntities(10, 10, 10)) {
							if(p instanceof Player) {
								Player near = (Player) p;
								if(team.getTeamMembers().contains(near.getUniqueId().toString())) {
									near.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 20 * 5, 1));
									near.sendMessage(ChatColor.YELLOW + player.getName() + " has just gave " + ChatColor.GOLD + "Strength Effect " + ChatColor.YELLOW + "for" + ChatColor.GOLD + " 5 Seconds.");
									
									player.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 20 * 5, 1));
			
									}
								}
							}
							
							
							if(player.getItemInHand().getAmount() > 1) {
								player.getItemInHand().setAmount(player.getItemInHand().getAmount() - 1);
							} else {
								player.setItemInHand(null);
							}
						}
					}
					
					//Speed
					if(player.getItemInHand().getType() == Material.SUGAR) {
						
						if(kitCooldown.containsKey(player.getName()) && kitCooldown.get(player.getName()) > System.currentTimeMillis()) {
							
							long timer = kitCooldown.get(player.getName()) - System.currentTimeMillis();
							player.sendMessage(ChatColor.RED.toString() + "Can't used Bard Effects for another " + ChatColor.BOLD + timer/1000 + "'s");
							
						} else {
							kitCooldown.put(player.getName(), System.currentTimeMillis() + (10 * 1000));
							player.sendMessage(ChatColor.AQUA + "Speed Effect Used -> " + ChatColor.RED + "Cooldown: 10 Seconds");
							player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 20 * 10, 2));
							for(Entity p : player.getNearbyEntities(10, 10, 10)) {
							if(p instanceof Player) {
								Player near = (Player) p;
								if(team.getTeamMembers().contains(near.getUniqueId().toString())) {
									near.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 20 * 10, 1));
									near.sendMessage(ChatColor.YELLOW + player.getName() + " has just gave " + ChatColor.GOLD + "Speed Effect " + ChatColor.YELLOW + "for" + ChatColor.GOLD + " 10 Seconds.");
									
									}
								}
							}
							
							if(player.getItemInHand().getAmount() > 1) {
								player.getItemInHand().setAmount(player.getItemInHand().getAmount() - 1);
							} else {
								player.setItemInHand(null);
							}
						}
					}
					
					//Fire Res
					if(player.getItemInHand().getType() == Material.MAGMA_CREAM) {
						
						if(kitCooldown.containsKey(player.getName()) && kitCooldown.get(player.getName()) > System.currentTimeMillis()) {
							
							long timer = kitCooldown.get(player.getName()) - System.currentTimeMillis();
							player.sendMessage(ChatColor.RED.toString() + "Can't used Bard Effects for another " + ChatColor.BOLD + timer/1000 + "'s");
							
						} else {
							kitCooldown.put(player.getName(), System.currentTimeMillis() + (20 * 1000));
							player.sendMessage(ChatColor.GOLD + "Fire Resistence Effect Used -> " + ChatColor.RED + "Cooldown: 20 Seconds");
							player.addPotionEffect(new PotionEffect(PotionEffectType.FIRE_RESISTANCE, 20 * 15, 1));
							for(Entity p : player.getNearbyEntities(10, 10, 10)) {
							if(p instanceof Player) {
								Player near = (Player) p;
								if(team.getTeamMembers().contains(near.getUniqueId().toString())) {
									near.addPotionEffect(new PotionEffect(PotionEffectType.FIRE_RESISTANCE, 20 * 15, 1));
									near.sendMessage(ChatColor.YELLOW + player.getName() + " has just gave " + ChatColor.GOLD + "Fire Resistence " + ChatColor.YELLOW + "for" + ChatColor.GOLD + " 15 Seconds.");
		
									}
								}
							}
							
							
							if(player.getItemInHand().getAmount() > 1) {
								player.getItemInHand().setAmount(player.getItemInHand().getAmount() - 1);
							} else {
								player.setItemInHand(null);
							}
						}
					}
				}
			}
		}
	}

}
