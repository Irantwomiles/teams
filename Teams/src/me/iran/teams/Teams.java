package me.iran.teams;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import me.iran.teams.events.TeamAttackPlayer;
import me.iran.teams.events.TeamChatEvent;
import me.iran.teams.events.TeamDisconnectEvent;
import me.iran.teams.events.TeamJoinEvent;
import me.iran.teams.events.TeamTeleportHomeEvent;
import me.iran.teams.kits.TeamRunnables;
import me.iran.teams.team.Claim;
import me.iran.teams.team.ClaimEvent;
import me.iran.teams.team.TeamKits;
import me.iran.teams.team.TeamManager;
import me.iran.teams.Teams;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

public class Teams extends JavaPlugin {

	//TODO: Create Team Duels
	//TODO: Create Team FFA
	//TODO: Create Team home /team home /team sethome
	
	File file = null;
	File tFile = null;
	public static HashMap<String, Integer> teleportHome = new HashMap<String, Integer>();
	TeamManager tm = new TeamManager(this);
	TeamRunnables tr = new TeamRunnables(this);
	public static ArrayList<String> teamChat = new ArrayList<String>();
	
	public void onEnable() {
		
		Bukkit.getPluginManager().registerEvents(new TeamJoinEvent(this), this);
		Bukkit.getPluginManager().registerEvents(new TeamDisconnectEvent(this), this);
		Bukkit.getPluginManager().registerEvents(new TeamChatEvent(this), this);
		Bukkit.getPluginManager().registerEvents(new TeamAttackPlayer(this), this);
		Bukkit.getPluginManager().registerEvents(new TeamKits(this), this);
		Bukkit.getPluginManager().registerEvents(new TeamTeleportHomeEvent(this), this);
		Bukkit.getPluginManager().registerEvents(new ClaimEvent(this), this);
		
		tr.runTaskTimer(this, 0L, 20L);
		
		tm.loadTeams();
		
		//Creating directory just for teams
		file = new File(this.getDataFolder(), "teams.yml");
		tFile = new File(this.getDataFolder()+ "/teams");
		
		if(!tFile.exists()) {
			System.out.println("[Teams] Couldn't Find any teams");
			file.mkdir();
			tFile = new File(this.getDataFolder() + "/Teams", "deleteme.yml");
			new YamlConfiguration();
			
			YamlConfiguration delete = YamlConfiguration.loadConfiguration(tFile);
			delete.createSection("Delete");
			
			delete.set("Delete", "Delete this file, but leave the folder!");
			try {
				delete.save(tFile);
			} catch (IOException e) {
				e.printStackTrace();
			}
			System.out.println("[Teams] Created the directory 'Teams' with no errors!");
		}
		
		if (!file.exists()) {
			file = new File(this.getDataFolder(), "teams.yml");
			
			new YamlConfiguration();
			
			YamlConfiguration config = YamlConfiguration.loadConfiguration(file);
			
			config.createSection("teams");
			try {
				config.save(file);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void onDisable() {
		tm.saveTeams();
	}
	
	public HashMap<String, Integer> getHomeTeleport() {
		return teleportHome;
	}
	
	public int getHomeTimer(Player player) {
		return teleportHome.get(player.getUniqueId().toString());
	}
	
	@SuppressWarnings("deprecation")
	public void teamMap(Player player) {
		for(me.iran.teams.team.Team team : tm.getAllTeams()) {
			
			int maxx = Math.max(team.getLoc1().getBlockX(), team.getLoc2().getBlockX());
			int minx = Math.min(team.getLoc1().getBlockX(), team.getLoc2().getBlockX());
			
			int maxz = Math.max(team.getLoc1().getBlockZ(), team.getLoc2().getBlockZ());
			int minz = Math.min(team.getLoc1().getBlockZ(), team.getLoc2().getBlockZ());
			
			
			for(int i = 4; i < 15; i++) {
				Location loc1 = new Location(Bukkit.getWorld("world"), minx, i, minz);
				Location loc2 = new Location(Bukkit.getWorld("world"), maxx, i, maxz);
				Location loc3 = new Location(Bukkit.getWorld("world"), maxx, i, minz);
				Location loc4 = new Location(Bukkit.getWorld("world"), minx, i, maxz);
				
				player.sendBlockChange(loc1, Material.MELON_BLOCK, (byte) 0);
				player.sendBlockChange(loc2, Material.MELON_BLOCK, (byte) 0);
				player.sendBlockChange(loc3, Material.MELON_BLOCK, (byte) 0);
				player.sendBlockChange(loc4, Material.MELON_BLOCK, (byte) 0);
			
			}
			
		}
	}
	
	@SuppressWarnings("deprecation")
	public void setBoard(Player player) {
		Scoreboard scoreboard;
		
		me.iran.teams.team.Team team = tm.getTeamByPlayer(player);
		
		
		if(player.getScoreboard() != Bukkit.getScoreboardManager().getMainScoreboard()) {
			scoreboard = player.getScoreboard();
		} else {
			scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
		}
		
		Team friend;
		
		if(scoreboard.getTeam("friend") != null) {
			friend = scoreboard.getTeam("friend");
		} else {
			friend = scoreboard.registerNewTeam("friend");
		}
		
		friend.setPrefix(ChatColor.GREEN + "");
		
		Team enemy;
		
		if(scoreboard.getTeam("enemy") != null) {
			enemy = scoreboard.getTeam("enemy");
		} else {
			enemy = scoreboard.registerNewTeam("enemy");
		}
		
		enemy.setPrefix(ChatColor.RED + "");
		enemy.setCanSeeFriendlyInvisibles(false);
		for(Player p : Bukkit.getOnlinePlayers()) {
				enemy.addPlayer(p);
			}
		if(tm.getAllTeams().contains(team)) {
			
			for(int i = 0; i < team.getTeamMembers().size(); i++) {
				
				for(Player p : Bukkit.getOnlinePlayers()) {
					if(team.getTeamMembers().contains(p.getUniqueId().toString())) {
						
						friend.addPlayer(p);
						friend.canSeeFriendlyInvisibles();
						
					}
				}
			}
		}
		
		player.setScoreboard(scoreboard);
	}
	
	
	@SuppressWarnings("deprecation")
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if(!(sender instanceof Player)) {
			sender.sendMessage(ChatColor.GOLD + "[Teams]" + ChatColor.RED + " Player Command Only!");
			return true;
		}
		
		Player player = (Player) sender;
		
		if(cmd.getName().equalsIgnoreCase("team")) {
			if(args.length < 1) {
				player.sendMessage(ChatColor.GOLD + "[Teams]"  + ChatColor.RED + " /team help for more info!");
				return true;
			}
			
			if(args[0].equalsIgnoreCase("help")) {
				player.sendMessage(ChatColor.GOLD.toString() + ChatColor.BOLD + "[Teams Help Guide]");
				player.sendMessage(ChatColor.RED + "--Commands for Team Owners--");
				player.sendMessage(ChatColor.GRAY + "- /team create <team name>");
				player.sendMessage(ChatColor.GRAY + "- /team disband");
				player.sendMessage(ChatColor.GRAY + "- /team rename <new name>");
				player.sendMessage(ChatColor.GRAY + "- /team promote <name>");
				player.sendMessage(ChatColor.GRAY + "- /team demote <name>");
				player.sendMessage(ChatColor.GRAY + "- /team leader <player> ");
				player.sendMessage(ChatColor.YELLOW + "--Commands for Team Captains--");
				player.sendMessage(ChatColor.GRAY + "- /team invite <player> ");
				player.sendMessage(ChatColor.GRAY + "- /team uninvite <player> ");
				player.sendMessage(ChatColor.GRAY + "- /team sethome");
				player.sendMessage(ChatColor.GRAY + "- /team kick <player> ");
				player.sendMessage(ChatColor.AQUA + "--Commands for Team Members--");
				player.sendMessage(ChatColor.GRAY + "- /team leave");
				player.sendMessage(ChatColor.GRAY + "- /team join <name>");
				player.sendMessage(ChatColor.GRAY + "- /team home");
				return true;
			}
			
			if(args[0].equalsIgnoreCase("create")) {
				if(args.length < 2) {
					player.sendMessage(ChatColor.GOLD + "[Teams]"  + ChatColor.RED + " /team help for more info!");
					return true;
				}
				tm.createTeam(args[1], player);
			}
			
			if(args[0].equalsIgnoreCase("claim")) {
				if(args.length < 1) {
					player.sendMessage(ChatColor.GOLD + "[Teams]"  + ChatColor.RED + " /team help for more info!");
					return true;
				}
				
				if(tm.isInTeam(player)) {
					me.iran.teams.team.Team team = tm.getTeamByPlayer(player);
					
					if(team.getTeamLeader().equals(player.getUniqueId().toString())) {
						ClaimEvent.claim.put(player.getName(), new Claim(null, null, team));
						player.getInventory().addItem(new ItemStack(Material.WOOD_HOE));
					}
					
				}
				
			}
			
			if(args[0].equalsIgnoreCase("map")) {
				if(args.length < 1) {
					player.sendMessage(ChatColor.GOLD + "[Teams]"  + ChatColor.RED + " /team help for more info!");
					return true;
				}
				
				teamMap(player);
				
			}

			if(args[0].equalsIgnoreCase("rename")) {
				if(args.length < 2) {
					player.sendMessage(ChatColor.GOLD + "[Teams]"  + ChatColor.RED + " /team help for more info!");
					return true;
				}
				
				me.iran.teams.team.Team team = tm.getTeamByPlayer(player);
				
				if(!tm.getAllTeams().contains(team)) {
					player.sendMessage(ChatColor.GOLD + "[Teams] " + ChatColor.RED + "You are not in a Team");
					return true;
				}
				
				if(!team.getTeamLeader().equals(player.getUniqueId().toString())) {
					player.sendMessage(ChatColor.GOLD + "[Teams] " + ChatColor.RED + "Only Leaders can do this command");
					return true;
				}
				
				String name = args[1];
				
				String[] notAllowed = { ",", ";", "!", "@", "#", "$",
						"%", "^", "&", "*", "(", ")", "+", "=", "`",
						"~", ".", "<", ">", "/", "\"", ":", ";", "{",
						"}", "?" };
				for(String no : notAllowed) {
					if (name.contains(no)) {
						player.sendMessage(ChatColor.GOLD + "[Teams]" + ChatColor.RED + " Invalid Characters in Team Name");
						return true;
					}
				}
				
				for(int i = 0; i < tm.getAllTeams().size(); i++) {
					me.iran.teams.team.Team t = tm.getAllTeams().get(i);
					
					if(t.getTeamName().equalsIgnoreCase(name)) {
						player.sendMessage(ChatColor.RED + "That name is already taken");
						return true;
					}
				}
				
				if(name.length() < 3 || name.length() > 12) {
					player.sendMessage(ChatColor.GOLD + "[Teams]" + ChatColor.RED + " Team name has to be atleast 3 letters but no more than 12");
					return true;
				}
				
				team.setTeamName(name);
				
				player.sendMessage(ChatColor.AQUA + "You have renamed your Team to " + name);
				
			}
			
			if(args[0].equalsIgnoreCase("invite")) {
				if(args.length < 2) {
					player.sendMessage(ChatColor.GOLD + "[Teams]"  + ChatColor.RED + " /team help for more info!");
					return true;
				}
				
				Player target = Bukkit.getPlayer(args[1]);
				
				if(target == null) {
					player.sendMessage(ChatColor.GOLD + "[Teams]"  + ChatColor.RED + " Invalid player");
					return true;
				}
				
				if(target == player) {
					player.sendMessage(ChatColor.GOLD + "[Teams]"  + ChatColor.RED + " You can't invite yourself");
					return true;
				}
				
				tm.inviteToTeam(player, target);
			}
			
			if(args[0].equalsIgnoreCase("uninvite")) {
				
				if(args.length < 2) {
					player.sendMessage(ChatColor.GOLD + "[Teams]"  + ChatColor.RED + " /team help for more info!");
					return true;
				}
				
				Player target = Bukkit.getPlayer(args[1]);
				
				if(target == null) {
					player.sendMessage(ChatColor.GOLD + "[Teams]"  + ChatColor.RED + " Invalid player");
					return true;
				}
				
				
				if(target == player) {
					player.sendMessage(ChatColor.GOLD + "[Teams]"  + ChatColor.RED + " You can't uninvite yourself");
					return true;
				}
				
				tm.unInvite(player, target);
			}
			
			if(args[0].equalsIgnoreCase("join")) {
				if(args.length < 2) {
					player.sendMessage(ChatColor.GOLD + "[Teams]"  + ChatColor.RED + " /team help for more info!");
					return true;
				}
				
				tm.joinTeam(player, args[1]);
			}
			
			if(args[0].equalsIgnoreCase("leave")) {
				tm.leaveTeam(player);
			}
			
			if(args[0].equalsIgnoreCase("disband")) {
				tm.disbandTeam(player);
			}
			
			if(args[0].equalsIgnoreCase("kick")) {
				if(args.length < 2) {
					player.sendMessage(ChatColor.GOLD + "[Teams]"  + ChatColor.RED + " /team kick <player>");
					return true;
				}
				
				Player target = Bukkit.getPlayer(args[1]);
				if(target == null) {
					player.sendMessage(ChatColor.GOLD + "[Teams]"  + ChatColor.RED + " Invalid player");
					return true;
				}
				
				tm.kickFromTeam(player, target);
			}
			
			if(args[0].equalsIgnoreCase("leader")) {
				if(args.length < 2) {
					player.sendMessage(ChatColor.GOLD + "[Teams]"  + ChatColor.RED + " /team leader <player>");
					return true;
				}
				
				Player target = Bukkit.getPlayer(args[1]);
				if(target == null) {
					player.sendMessage(ChatColor.GOLD + "[Teams]"  + ChatColor.RED + " Invalid player");
					return true;
				}
				
				tm.makeLeader(player, target);
			}
			
			if(args[0].equalsIgnoreCase("info")) {
				if(args.length < 2) {
					tm.getTeamInfoByPlayer(player, player);
					return true;
				}
				
				Player target = Bukkit.getPlayer(args[1]);
				
				if(target == null) {
					tm.getTeamInfoByName(player, args[1]);
				} else {
					tm.getTeamInfoByPlayer(player, target);
				}
				
				
			}
			
			if(args[0].equalsIgnoreCase("chat")) {
				
				if (tm.isInTeam(player) == true) {
					if (teamChat.contains(player.getName())) {
						teamChat.remove(player.getName());
						player.sendMessage(ChatColor.YELLOW + "You are now speaking in Public chat");
					} else {
						teamChat.add(player.getName());
						player.sendMessage(ChatColor.GREEN + "You are now speaking in Team chat");

					}
				} else {
					player.sendMessage(ChatColor.GOLD + "[Teams] " + ChatColor.RED + "You are not in a Team");
				}

			}
			
			if(args[0].equalsIgnoreCase("sethome")) {
			
				me.iran.teams.team.Team team = tm.getTeamByPlayer(player);
				
				if(!tm.getAllTeams().contains(team)) {
					player.sendMessage(ChatColor.GOLD + "[Teams] " + ChatColor.RED + "You are not in a Team");
					return true;
				}
				
				if(!team.getTeamLeader().equals(player.getUniqueId().toString()) && !team.getTeamCaptains().contains(player.getUniqueId().toString())) {
					player.sendMessage(ChatColor.GOLD + "[Teams] " + ChatColor.RED + "You must be a Leader or Captain to perform this command");
					return true;
				}
				
				
				int x = player.getLocation().getBlockX();
				int y = player.getLocation().getBlockY();
				int z = player.getLocation().getBlockZ();
				float pitch = player.getLocation().getPitch();
				float yaw = player.getLocation().getYaw();
				String world = player.getLocation().getWorld().getName();
				
				team.setHomeX(x);
				team.setHomeY(y);
				team.setHomeZ(z);
				team.setHomePitch(pitch);
				team.setHomeYaw(yaw);
				team.setHomeWorld(world);
				
				for(Player p : Bukkit.getOnlinePlayers()) {
					if(team.getTeamMembers().contains(p.getUniqueId().toString())) {
						p.sendMessage(player.getName() + " has just set a Team home");
					}
				}
				
			}
			
			if(args[0].equalsIgnoreCase("home")) {
			
				me.iran.teams.team.Team team = tm.getTeamByPlayer(player);
				
				if(!tm.getAllTeams().contains(team)) {
					player.sendMessage(ChatColor.GOLD + "[Teams] " + ChatColor.RED + "You are not in a Team");
					return true;
				}
				
				if(team.getHomeX() == 0 && team.getHomeY() == 0 && team.getHomeZ() == 0 && team.getHomePitch() == 0 && team.getHomeYaw() == 0) {
					player.sendMessage(ChatColor.GOLD + "[Teams] " + ChatColor.RED + "Your Team has not set a home yet!");
					return true;
				}
				
				teleportHome.put(player.getUniqueId().toString(), 10);
			}
			
			
			if(args[0].equalsIgnoreCase("promote")) {
				
				if(args.length < 2) {
					player.sendMessage(ChatColor.GOLD + "[Teams]"  + ChatColor.RED + " /team promote <player>");
					return true;
				}
				
				me.iran.teams.team.Team team = tm.getTeamByPlayer(player);
				
				Player target = Bukkit.getPlayer(args[1]);
				
				if(target == null) {
					player.sendMessage(ChatColor.GOLD + "[Teams]"  + ChatColor.RED + " Invalid player");
					return true;
				}
				
				if(target == player) {
					player.sendMessage(ChatColor.GOLD + "[Teams]"  + ChatColor.RED + " You can't captain yourself");
					return true;
				}
				
				if(team.getTeamCaptains().contains(target.getUniqueId().toString())) {
					player.sendMessage(ChatColor.GOLD + "[Teams]"  + ChatColor.RED + " That player is already a Captain");
					return true;
				}
				
				if(!team.getTeamMembers().contains(target.getUniqueId().toString())) {
					player.sendMessage(ChatColor.GOLD + "[Teams]"  + ChatColor.RED + " That player is not in your Team");
					return true;
				}
				
				if(!team.getTeamLeader().equals(player.getUniqueId().toString())) {
					player.sendMessage(ChatColor.GOLD + "[Teams]"  + ChatColor.RED + " You must be a Leader to perform this Command");
					return true;
				}
				
				team.addCaptain(target.getUniqueId().toString());
				player.sendMessage(ChatColor.AQUA + "You've made " + target.getName() + " a Team Captain");
				target.sendMessage(ChatColor.AQUA + "You are a Team Captain now");
			}
			
			if(args[0].equalsIgnoreCase("demote")) {
				if(args.length < 2) {
					player.sendMessage(ChatColor.GOLD + "[Teams]"  + ChatColor.RED + " /team demote <player>");
					return true;
				}
			
				me.iran.teams.team.Team team = tm.getTeamByPlayer(player);
				
				if(!tm.getAllTeams().contains(team)) {
					player.sendMessage(ChatColor.GOLD + "[Teams]"  + ChatColor.RED + " You are not in a Team");
					return true;
				}
				
				Player target = Bukkit.getPlayer(args[1]);
				
				if(target == null) {
					player.sendMessage(ChatColor.GOLD + "[Teams]"  + ChatColor.RED + " Invalid player");
					return true;
				}
				
				if(target == player) {
					player.sendMessage(ChatColor.GOLD + "[Teams]"  + ChatColor.RED + " You can't demote yourself");
					return true;
				}
				
				if(!team.getTeamLeader().equals(player.getUniqueId().toString())) {
					player.sendMessage(ChatColor.GOLD + "[Teams]"  + ChatColor.RED + " You must be a Leader to perform this Command");
					return true;
				}
				
				if(!team.getTeamMembers().contains(target.getUniqueId().toString())) {
					player.sendMessage(ChatColor.GOLD + "[Teams]"  + ChatColor.RED + " That player is not in your Team");
					return true;
				}
				
				if(!team.getTeamCaptains().contains(target.getUniqueId().toString())) {
					player.sendMessage(ChatColor.GOLD + "[Teams]"  + ChatColor.RED + " That player is not a captain");
					return true;
				}
				
				team.removeCaptain(target.getUniqueId().toString());
				
				player.sendMessage(ChatColor.RED + "You have demoted " + target.getName() + " from Captain to a Member");
				target.sendMessage(ChatColor.RED + "You have been demoted from Captain to Member");
			}
			
			if(args[0].equalsIgnoreCase("loc1")) {
				me.iran.teams.team.Team team = tm.getTeamByPlayer(player);
				
				team.setLoc1(player.getLocation());
			}
			
			if(args[0].equalsIgnoreCase("loc2")) {
				me.iran.teams.team.Team team = tm.getTeamByPlayer(player);
				
				team.setLoc2(player.getLocation());
			}
			
		}
		
		return true;
	}

}
