package me.iran.teams.events;

import me.iran.teams.Teams;
import me.iran.teams.team.Team;
import me.iran.teams.team.TeamManager;
import net.md_5.bungee.api.ChatColor;

import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

public class TeamAttackPlayer implements Listener {

	Teams plugin;
	TeamManager tm = new TeamManager(plugin);
	
	public TeamAttackPlayer(Teams plugin) {
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onBreak(BlockBreakEvent event) {
		Player player = event.getPlayer();
		Block block = event.getBlock();
		
		Team team = tm.insideClaim(block.getLocation());
		
		if(team != null && !team.getTeamLeader().equals(player.getUniqueId().toString())) {
			event.setCancelled(true);
			player.sendMessage(ChatColor.RED + "Can't break in the territory of " + ChatColor.LIGHT_PURPLE + team.getTeamName());
		}
	}
	
	@EventHandler
	public void onAttack(EntityDamageByEntityEvent event) {
		if(event.getEntity() instanceof Player && event.getDamager() instanceof Player) {
			Player hit = (Player) event.getEntity();
			Player damager = (Player) event.getDamager();
			
			Team hitTeam = tm.getTeamByPlayer(hit);
			Team damTeam = tm.getTeamByPlayer(damager);
			
			if(tm.getAllTeams().contains(hitTeam) && tm.getAllTeams().contains(damTeam)) {
				if(hitTeam.getTeamName().equalsIgnoreCase(damTeam.getTeamName())) {
					event.setCancelled(true);
				}
			}
		}
	}
}
