package me.iran.teams.events;

import me.iran.teams.Teams;

import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

public class TeamTeleportHomeEvent implements Listener {

	Teams plugin;
	
	public TeamTeleportHomeEvent(Teams plugin) {
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onMove(PlayerMoveEvent event) {
		
		Player player = event.getPlayer();
		
		if(Teams.teleportHome.containsKey(player.getUniqueId().toString())) {
			if(!(event.getFrom().getBlockX() == event.getTo().getBlockX() && event.getFrom().getBlockY() == event.getTo().getBlockY() && event.getFrom().getBlockZ() == event.getTo().getBlockZ())) {
				Teams.teleportHome.remove(player.getUniqueId().toString());
				player.sendMessage(ChatColor.RED + "You have moved and teleportation has been Stopped");
				player.playSound(player.getLocation(), Sound.ANVIL_BREAK, 10.0f, 10.0f);
			}
		}
	}
}
