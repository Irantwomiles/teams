package me.iran.teams.events;

import me.iran.teams.Teams;
import me.iran.teams.team.Team;
import me.iran.teams.team.TeamManager;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class TeamJoinEvent implements Listener {

	Teams plugin;
	
	TeamManager tm = new TeamManager(plugin);
	
	public TeamJoinEvent (Teams plugin) {
		this.plugin = plugin;
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onJoin(PlayerJoinEvent event) {
		Player player = event.getPlayer();
		
		Team team = tm.getTeamByPlayer(player);
		
		if(!tm.getAllTeams().contains(team)) {
			player.sendMessage(ChatColor.GOLD + "[Teams]" + ChatColor.RED + " Seems like you don't have a Team, /team help");
			return;
		}
		
		for(Player p : Bukkit.getOnlinePlayers()) {
			if(team.getTeamMembers().contains(p.getUniqueId().toString())) {
				p.sendMessage(ChatColor.YELLOW + player.getName() + " is " + ChatColor.GREEN + "Online");
			}
		}
		
		event.setJoinMessage(null);
	}
}
