package me.iran.teams.kits;

import me.iran.teams.Teams;
import me.iran.teams.team.TeamManager;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class TeamRunnables extends BukkitRunnable {
	
	Teams plugin;
	
	public TeamRunnables(Teams plugin) {
		this.plugin = plugin;
	}
	
	TeamManager tm = new TeamManager(plugin);
	
	Bard bard = new Bard();
	Miner miner = new Miner();
	int saveTimer = 20 * 60 * 30;
	
	@SuppressWarnings("deprecation")
	public void run() {
		
		saveTimer--;
		
		if(saveTimer == 0) {
			tm.saveTeams();
			System.out.println("[Teams] Saved Teams");
			saveTimer = 20 * 60 * 30;
		}
		
		for(Player p : Bukkit.getOnlinePlayers()) {
		//	bard.bard(p);
			miner.miner(p);
			plugin.setBoard(p);
			
			if(plugin.getHomeTeleport().containsKey(p.getUniqueId().toString())) {
				p.sendMessage(ChatColor.GOLD + "Teleporting home in: " + plugin.getHomeTeleport().get(p.getUniqueId().toString()));
				plugin.getHomeTeleport().put(p.getUniqueId().toString(), plugin.getHomeTeleport().get(p.getUniqueId().toString()) - 1);
				p.playSound(p.getLocation(), Sound.NOTE_BASS_GUITAR, 10.0f, 10.0f);
				if(plugin.getHomeTeleport().get(p.getUniqueId().toString()) == 0) {
					int x = tm.getTeamByPlayer(p).getHomeX();
					int y = tm.getTeamByPlayer(p).getHomeY();
					int z = tm.getTeamByPlayer(p).getHomeZ();
					float pitch = tm.getTeamByPlayer(p).getHomePitch();
					float yaw = tm.getTeamByPlayer(p).getHomeYaw();
					String world = tm.getTeamByPlayer(p).getHomeWorld();
					Location home = new Location(Bukkit.getWorld(world), x, y, z);
					home.setPitch(pitch);
					home.setYaw(yaw);
					p.teleport(home);
					plugin.getHomeTeleport().remove(p.getUniqueId().toString());
					p.playSound(p.getLocation(), Sound.PORTAL, 10.0f, 10.0f);
					p.sendMessage(ChatColor.GOLD + "Teleported to Team home!");
				}
			}
		}
	}

}
