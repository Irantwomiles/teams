package me.iran.teams.kits;

import java.util.HashSet;
import java.util.Set;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class Miner {
	
	static Set<String> checkMiner = new HashSet<String>();
	
	public void miner(Player player) {
		ItemStack helmet = player.getInventory().getHelmet();
		ItemStack chest = player.getInventory().getChestplate();
		ItemStack leggings = player.getInventory().getLeggings();
		ItemStack boots = player.getInventory().getBoots();

		if(helmet != null && chest != null && leggings != null && boots != null) {
			if(helmet.getType() == Material.IRON_HELMET && chest.getType() == Material.IRON_CHESTPLATE && leggings.getType() == Material.IRON_LEGGINGS && boots.getType() == Material.IRON_BOOTS) {
				player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 1000000, 1));
				player.addPotionEffect(new PotionEffect(PotionEffectType.FAST_DIGGING, 1000000, 1));
				player.addPotionEffect(new PotionEffect(PotionEffectType.NIGHT_VISION, 1000000, 0));
				checkMiner.add(player.getName());
				if(player.getLocation().getBlockY() <= 15) {
					player.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 1000000, 0));
				} else {
					player.removePotionEffect(PotionEffectType.INVISIBILITY);
				}
				
				
			}
		} else if (checkMiner.contains(player.getName())) {
			if((helmet == null || chest == null || leggings == null || boots != null)) {
				player.removePotionEffect(PotionEffectType.SPEED);
				player.removePotionEffect(PotionEffectType.FAST_DIGGING);
				player.removePotionEffect(PotionEffectType.NIGHT_VISION);
				checkMiner.remove(player.getName());
			}

		}
	}
	
	public Set<String> getMiner() {
		return checkMiner;
	}
	
	public int getMinerSize() {
		return checkMiner.size();
	}
}
