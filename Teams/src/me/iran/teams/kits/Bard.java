package me.iran.teams.kits;

import java.util.HashSet;
import java.util.Set;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class Bard {

	static Set<String> checkBard = new HashSet<String>();
	
	public void bard(Player player) {
		ItemStack helmet = player.getInventory().getHelmet();
		ItemStack chest = player.getInventory().getChestplate();
		ItemStack leggings = player.getInventory().getLeggings();
		ItemStack boots = player.getInventory().getBoots();
			if(helmet != null && chest != null && leggings != null && boots != null) {
				if (helmet.getType() == Material.GOLD_HELMET && chest.getType() == Material.GOLD_CHESTPLATE && leggings.getType() == Material.GOLD_LEGGINGS && boots.getType() == Material.GOLD_BOOTS) {
					player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 100000000, 1));
					player.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 100000000, 0));
					player.addPotionEffect(new PotionEffect(PotionEffectType.NIGHT_VISION, 100000000, 0));
					checkBard.add(player.getName());
				}
			} else if (checkBard.contains(player.getName())) {
				if((helmet == null || chest == null || leggings == null || boots != null)) {
					player.removePotionEffect(PotionEffectType.SPEED);
					player.removePotionEffect(PotionEffectType.DAMAGE_RESISTANCE);
					player.removePotionEffect(PotionEffectType.NIGHT_VISION);
					checkBard.remove(player.getName());
			}

		}

	}
	
	public Set<String> getBard() {
		return checkBard;
	}
	
	public int getBardSize() {
		return checkBard.size();
	}
	
}
